#
#     setup_headers - Sets a standard license header in all source files
#
#     Copyright (C) 2019 Jorge M. Faleiro Jr.
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as published
#     by the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
#
#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
.PHONY: test coverage deploy.staging deploy.production deploy headers
test:
	mkdir -p build
	python -m pytest --durations=0 -vv
headers:
	./setup.py adjust_license_headers
coverage:
	mkdir -p build
	coverage run --omit='tests/*' --source setup_headers -m pytest
	coverage report -m
	coverage html -d build/coverage/html
deploy.staging:
	python setup.py bdist_wheel
	twine upload --repository-url https://test.pypi.org/legacy/ dist/*
deploy.production:
	python setup.py bdist_wheel
	twine upload dist/*
deploy:
	bin/deploy.sh
prepare:
	# bin/tag-version.sh
	poetry install -E coverage -E tests -E interactive-dev
clean:
	rm -rf .tox build .pytest_cache
version:
	scmversion tag-version
